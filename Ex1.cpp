/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 *
 * Description : Ex1 of lab2, practice for function lession
 * Task		   : Xay dung ham tinh tich cua 2 ma tran M x N v� N x K. Viet chuong trinh vi du
 *
 * Date        : 11/4/2015
 *
 */

#include <iostream>
#define MAX 100

using namespace std;

void enterMatrix ( int a[][MAX] , int numOfRow, int numOfColunm ) {
    cout << "Enter the matrix' s value \n";
    for(int i = 0; i < numOfRow; i++) {
        for(int j = 0; j < numOfColunm; j++) {
            cin >> a[i][j];
        }
    }
}
 
void printMatrix ( int a[][MAX], int numOfRow, int numOfColunm) {
    for(int i = 0; i < numOfRow ; i++) {
        for(int j = 0; j < numOfColunm; j++) {
            cout << a[i][j] << " "; 
        }
    	cout << "\n";
    }
}
 

void multi ( int a[][MAX], int b[][MAX], int temp[][MAX], int rA, int cA, int cB ) {
    for(int i = 0; i < rA; i++) {
        for(int j = 0; j < cB; j++) {
            temp[i][j]=0;
            for(int k = 0; k < cA; k++) {
                temp[i][j] += a[i][k] * b[k][j];
            }
        }
    }
}
 
int main() {
    int a[MAX][MAX], b[MAX][MAX], temp[MAX][MAX];
    int rA, cA, cB, cTemp, rTemp;
    
    cout << "Number row of A: ";
    cin >> rA;
    cout << "Number colunm of A: ";
    cin >> cA;
  
    enterMatrix (a, rA, cA);

    cout << "Number colunm of B: ";
    cin >> cB;
    
    enterMatrix (b, cA, cB);
 
    multi ( a, b, temp, rA, cA, cB);
 
    rTemp = rA;
    cTemp = cB;
    
    cout << "Multi of A and B: \n";
    printMatrix (temp, rTemp, cTemp);
 
    return 0;
}
