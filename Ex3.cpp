/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 *
 * Description : Ex3 of lab2, practice for function lession
 * Task		   : Viet chuong trinh liet ke tat ca cac hoan vi cua mot day so nguyen cho truoc
 *
 * Reference : http://www.nguyenvanquan7826.com/2013/09/03/thuat-toan-liet-ke-hoan-vi/
 * Date        : 11/4/2015
 *
 */

#include <iostream>
#include <cmath>
#define MAX 100

using namespace std;

int n, a[MAX];

void sortIncrease () {
	for (int i = 0; i < n - 1; i++) {
		for (int j = i; j < n; j++) {
			if (a[i] > a[j]) swap(a[i], a[j]);
		}
	}
} 

// Enter the list and sort it increasingly
void init() {
	cout << "Enter the size of list: ";
	cin >> n;
	cout << "Enter " << n << " element of this list: ";
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
	sortIncrease ();
	
}

void print() {
    for (int k = 0; k < n; k++) {
        cout << a[k] << " ";
    }
    cout<< endl;
}

void conversion () {
	print(); // print the order
 	int i = n-1;        // i la vi tri ma doan cuoi giam dan
    while (i > 0) {  // trong khi doan cuoi khong phai bat dau tu vi tri dau tien
        i = n-1;
        while (a[i] < a[i-1]) {  // tim vi tri dau tien ma doan cuoi giam dan
            i--; 
        }        
        if (i>0) { // Neu tim duoc i
            int k;      // tim so a[k] nho nhat trong doan giam dan ma a[k] > a[i-1]
            for (k = n-1; k >= i; k--) {
                if (a[k] >= a[i-1])       
                    break;
            }
            swap(a[k], a[i - 1]);     // hoan vi a[k] va a[i-1]
             
            // dao nguoc doan cuoi giam dan thanh tang dan
            for (int k = i; k < i + (n - i) / 2; k++) {
            	if (a[k] != a[n - 1 - k +i])
                	swap(a[k], a[n - 1 - k + i]);
//                else goto tagDup;
            }
             
			print();
//			tagDup: ;
        }
    }	
}
 
int main() {
	init();
    conversion();
    return 0; 
}
