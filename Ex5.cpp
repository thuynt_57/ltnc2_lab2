/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 *
 * Description : Ex5 of lab2, practice for function lession
 * Task		   : Sap xep day so nguyen theo phuong phap tron
 *
 * Date        : 11/4/2015
 *
 */

#include<math.h>
#include<iostream>
#include<conio.h>
#define MAX 100

using namespace std;


void merge(int num[], int left, int mid, int right) {
	int tmp[right-left+1];
	int i1 = left;
	int i2 = mid+1;
	int x = 0;
	
	while (i1 < mid+1 && i2 < right+1) {
		if (num[i1] <= num[i2]){
			tmp[x++] = num[i1++];
		}
		else {
			tmp[x++] = num[i2++];
		}
	}
	
	while (i1 < mid+1) tmp[x++] = num[i1++];
	while (i2 < right+1) tmp[x++] = num[i2++];
	
    for(int i=0; i<=right-left; i++){
        num[left+i] = tmp[i];
    }
    //delete[] tmp;
}

void mergeSort(int num[], int left, int right){
	if (left < right){
		int mid = (left+right)/2;
		
		mergeSort (num, left, mid);
		mergeSort (num, mid+1, right);
		merge (num, left, mid, right);
	}
}

void mergeSort(int num[], int n){
	mergeSort(num, 0, n-1);
}

int main(){
	int num[MAX], n=0;
	
	cout << "Enter the size of list: ";
	cin >> n;
	cout << "Enter " <<  n << " elements: ";
	for (int i = 0; i < n; i++) {
		cin >> num[i];
	}	

	mergeSort (num, n);
	
	cout << "After sorting we have a increase list like this: "; 
	for (int i = 0; i < n; i++ ) {
		cout << num[i] << " ";
	}
	cout << endl;
	return 0;
}
