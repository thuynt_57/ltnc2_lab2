/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 *
 * Description : Ex4 of lab2, practice for function lession
 * Task		   : Viet chuong trinh liet ke tat ca cac day nhi phan co do dai K
 *
 * Date        : 11/4/2015
 *
 */

#include <iostream>
#include <cmath>
#define MAX 100

using namespace std;

void print ( int n , int a[] ) {
    for(int j = 0; j < n; j++)
    	cout << a[j];
    cout << endl;
 
}

void binary ( int i , int n, int a[] ) {
	int k;
	for ( k = 0; k <= 1; k++) {
	   	a[i] = k;
	    if ( i ==  n - 1 )
	        print ( n, a );
	    else
	        binary ( i+1, n, a );
	}
}
 
int main () {
	int n, a[MAX];
	cout << "Enter the length of list: ";
	cin >> n;
	binary ( 0, n, a );
	return 0;
}


