/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 
 * Description : Ex6 b of lab2, practice for function lession
 * Task		   : Liet ke tat ca cac cach dat n quan hau tren ban co vua n x n sao cho n quan hau khong an nhau
 			     
 * Reference   : http://nguyentung.name.vn/bai-toan-xep-hau-backtracking/
 
 * Date        : 16/4/2015
 
 */

#include <iostream>
#include <cmath>
#define MAX 100

using namespace std;

// a[] : used to store the result
// n is the size of chess table
// d[i] = 1 : column at index i is free, a[i] = 0 : column at index i is attacked by a queen.
// b[i] = 1 :  super diagonal  at index i is free, b[i] = 0 :  super diagonal  at index i is attacked by a queen.
// c[i] = 1 : subdiagonal at index i is free, b[i] = 0 : subdiagonal at index i is attacked by a queen.
int a[MAX], n, b[MAX], c[MAX], d[MAX];
 
// Firstly, init array b, c, d with value 1, that mean all cell is free.
void init () {
	cout << "Enter the size of chess table: ";
    cin >> n;
    if ( n <=3 ) cout << "n must be greater than 3 for this task \n";
    else {
		for( int i = 2; i <= 2*n; i++)
		b[i] = 1;
		for( int i = 1-n; i <= n-1; i++)
		c[i] = 1;
		for( int i = 1; i <= n; i++)
		d[i] = 1;
    }

}
 
// Show the result, each pair (i, a[i]) is the index u can put your queen on the chess table
void output () {
	cout << "The cells u can put your queen are:" ;
    for( int i = 1; i <= n; i++)
        cout << "{" << i << "," << a[i] << "} ";
    cout << endl;
}

// backtrack with row i
void backTrack ( int i ) {
    for ( int j = 1; j <= n; j++) { // try to put a queen each column
        if ( d[j] && b[i+j] && c[i-j] ) { // if this colum is not attacked by any queen
            a[i] = j; //  put a queen at this index
            d[j] = 0; b[i+j] = 0;c[i-j] = 0; // set value '0' for each cell that are attacked by this queen
            if( i == n ) output(); // if we reach the last row, show the result
            else backTrack ( i+1 ); // else continue with the next row
            d[j] = 1; b[i+j] = 1; c[i-j] = 1; // reset value '1' to find other solutions
        }
    }
}
 
int main () {
    init();
    backTrack(1);
    return 0;
}
