/* Author      : Nguyen Thi Thuy
 * Email       : ngthuy1511@gmail.com
 * Course      : Advance in programing
 *
 * Description : Ex2 of lab2, practice for function lession
 * Task		   : Viet ham tinh uoc chung lon nhat cua N so nguyen duong
 *
 * Date        : 11/4/2015
 *
 */

#include <iostream>
#include <cmath>
#define MAX 100

using namespace std;

int greatestCommonDivisor2 ( int a, int b ) {
	int temp;
	while( ( temp = (a%b ) ) != 0)  {
        a = b;
        b = temp;
    }
    return b;
}

int greatestCommonDivisorN ( int a[], int n ) {
	if ( n <= 1 ) {
		cout << "Give me more number!!!";
		return -1; 
	}
	else if ( n == 2 ) return  greatestCommonDivisor2 ( a[0], a[1] );
	else {
		a[n-2] = greatestCommonDivisor2 ( a[n - 1], a[n - 2] );
		return greatestCommonDivisorN ( a, n - 1 );
	}
}
 
int main() {
    int a[MAX], n;
    cout << "Enter the length of list: ";
    cin >> n;
    cout << "Enter the list of number: ";
    for( int i =0; i < n; i++)
    	cin >> a[i];
 
 	cout << "The greatest common divisor of the given list below is: "<< greatestCommonDivisorN ( a, n );
    return 0;
}
